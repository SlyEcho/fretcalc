package main

type Chord struct {
	Name      string
	Intervals []int
}

type ChordList []*Chord

func NewChord(name string, intervals ...int) *Chord {
	return &Chord{name, intervals}
}

func (c *Chord) GetNotes(root *Note) NoteList {
	notes := make(NoteList, len(c.Intervals))

	for i, n := range c.Intervals {
		notes[i] = Notes.GetNote(root.Scale + n)
	}

	return notes
}

//Fb   Cb   Gb   Db   Ab   Eb   Bb   F    C    G    D    A    E    B    F#   C#   G#   D#   A#   E#   B#
//-7   -6   -5   -4   -3   -2   -1   0    1    2    3    4    5    6     7    8    9   10   11   12   13

var (
	Major                = NewChord("", 0, 4, 1)
	Augmented            = NewChord("+", 0, 4, 8)
	Minor                = NewChord("m", 0, -3, 1)
	Dim                  = NewChord("dim", 0, -3, -6)
	DimSixth             = NewChord("dim6", 0, -3, -6, 3)
	DimSeventh           = NewChord("dim7", 0, -3, -6, -9)
	MinorSixth           = NewChord("m6", 0, -3, 1, 3)
	Sixth                = NewChord("6", 0, 4, 1, 3)
	Seventh              = NewChord("7", 0, 4, 1, -2)
	SeventhSharpFive     = NewChord("7#5", 0, 4, 8, -2)
	SeventFlatNine       = NewChord("7b9", 0, 4, 1, -2, -5)
	MajorSeventh         = NewChord("M7", 0, 4, 1, 5)
	MinorSeventh         = NewChord("m7", 0, -3, 1, -2)
	MinorSeventhFlatFive = NewChord("m7b5", 0, -3, -6, -2)
	MinorMajorSeventh    = NewChord("mM7", 0, -3, 1, 5)
	Ninth                = NewChord("9", 0, 4, 1, -2, 2)
	AddNine              = NewChord("add9", 0, 4, 1, 2)

	AllChords = ChordList{
		Major,
		Augmented,
		Minor,
		Dim,
		DimSixth,
		DimSeventh,
		MinorSixth,
		Sixth,
		Seventh,
		SeventhSharpFive,
		SeventFlatNine,
		MajorSeventh,
		MinorSeventh,
		MinorSeventhFlatFive,
		MinorMajorSeventh,
		Ninth,
		AddNine,
	}
)
