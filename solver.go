package main

import "sort"

type Solver struct {
	Frets *Fretboard

	MinFret      int
	MaxFret      int
	MaxDist      int
	RootOptional bool

	results       SolutionList
	chordPitches  []int
	stringPitches []int
	positions     Solution

	stringPitchesFound bitmap
	chordPitchesFound  bitmap
}

func NewSolver(frets *Fretboard) *Solver {
	return &Solver{
		Frets: frets,

		MinFret:      0,
		MaxFret:      frets.FretCount,
		MaxDist:      frets.FretCount,
		RootOptional: true,

		positions:     make(Solution, frets.StringCount()),
		stringPitches: frets.Strings.GetPitches(),

		stringPitchesFound: make(bitmap, frets.StringCount()),
	}
}

func (s *Solver) GetFingeringsByNote(root *Note, chord *Chord) []Solution {
	return s.GetFingerings(chord.GetNotes(root))
}

func (s *Solver) GetFingerings(chordNotes NoteList) []Solution {
	s.results = make(SolutionList, 0, 10)
	s.chordPitches = chordNotes.GetPitches()
	s.chordPitchesFound = make(bitmap, len(s.chordPitches))

	s.findSolutions(0)

	if len(s.chordPitches) > 4 && s.RootOptional {
		s.chordPitches = s.chordPitches[1:]
		s.chordPitchesFound = make(bitmap, len(s.chordPitches))

		s.findSolutions(0)
	}

	sort.Sort(s.results)

	return s.results
}

func (s *Solver) findSolutions(str int) {
	s.stringPitchesFound[str] = false

	for i := s.MinFret; i < s.MaxFret; i++ {
		s.positions[str] = i

		if i != 0 {
			dist := 0
			for x := 0; x < str; x++ {
				if nd := abs(s.positions[x] - i); s.positions[x] != 0 && nd > dist {
					dist = nd
				}
			}

			if dist > s.MaxDist {
				continue
			}
		}

		s.stringPitches[str] = (s.Frets.Strings[str].Pitch + i) % 12

		if str == s.Frets.StringCount()-1 {
			if s.contains() {
				s.results.Add(s.positions.Clone())
			}
		} else {
			s.findSolutions(str + 1)
		}
	}
}

func (s *Solver) contains() bool {

	s.stringPitchesFound.clear()
	s.chordPitchesFound.clear()

	for i := 0; i < len(s.chordPitches); i++ {
		s.chordPitchesFound[i] = false

		for j := 0; j < len(s.stringPitches); j++ {
			if s.chordPitches[i] == s.stringPitches[j] {
				s.stringPitchesFound[j] = true
				s.chordPitchesFound[i] = true
			}
		}
	}

	if !s.stringPitchesFound.all() || !s.chordPitchesFound.all() {
		return false
	}

	return true
}
