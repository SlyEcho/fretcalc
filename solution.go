package main

type Solution []int

func (s Solution) total() (max int, sum int) {
	for i, max, sum := 0, 0, 0; i < len(s); i++ {
		if s[i] > max {
			max = s[i]
		}
		sum += s[i]
	}
	return
}

func (s Solution) Clone() Solution {
	p := make(Solution, len(s))
	copy(p, s)
	return p
}

type SolutionList []Solution

func (s SolutionList) Len() int      { return len(s) }
func (s SolutionList) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s SolutionList) Less(i, j int) bool {
	var im, is = s[i].total()
	var jm, js = s[j].total()

	if im == jm {
		return is < js
	} else {
		return im < jm
	}
}

func (sl *SolutionList) Add(s Solution) { *sl = append(*sl, s) }
