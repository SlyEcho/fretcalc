package main

type Note struct {
	Pitch int
	Name  string
	Scale int
}

type NoteList []*Note

func (notes NoteList) GetPitches() []int {
	pitches := make([]int, len(notes))
	for i, n := range notes {
		pitches[i] = n.Pitch
	}
	return pitches
}

func (n *Note) String() string {
	return n.Name
}

func (s NoteList) Len() int           { return len(s) }
func (s NoteList) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s NoteList) Less(i, j int) bool { return s[i].Scale < s[j].Scale }
func (s *NoteList) Add(n *Note)       { *s = append(*s, n) }
