package main

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

type bitmap []bool

func (b bitmap) clear() {
	for i := 0; i < len(b); i++ {
		b[i] = false
	}
}

func (b bitmap) all() bool {
	for i := 0; i < len(b); i++ {
		if b[i] == false {
			return false
		}
	}
	return true
}
