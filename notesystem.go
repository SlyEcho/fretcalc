package main

import "sort"

type NoteSystem struct {
	ByScale map[int]*Note
	ByName  map[string]*Note
	All     NoteList
}

var Notes = CreateSystem()

func CreateSystem() *NoteSystem {

	system := new(NoteSystem)

	system.ByScale = make(map[int]*Note, 100)
	system.ByName = make(map[string]*Note, 100)
	system.All = make(NoteList, 0, 100)

	const (
		flatFlat   = "bb"
		flat       = "b"
		sharp      = "#"
		sharpSharp = "x"
		n          = "CDEFGAB"
	)

	var (
		scales  = [7]int{1, 3, 5, 0, 2, 4, 6}
		pitches = [7]int{0, 2, 4, 5, 7, 9, 11}
	)

	for i := 0; i < len(n); i++ {
		p := pitches[i]
		s := scales[i]
		c := string(n[i])

		system.addNote((p+10)%12, c+flatFlat, s-14)
		system.addNote((p+11)%12, c+flat, s-7)
		system.addNote((p+0)%12, c, s)
		system.addNote((p+1)%12, c+sharp, s+7)
		system.addNote((p+2)%12, c+sharpSharp, s+14)
	}

	sort.Sort(system.All)

	return system
}

func (notes *NoteSystem) addNote(pitch int, name string, scale int) {
	n := &Note{pitch, name, scale}
	notes.ByScale[scale] = n
	notes.ByName[name] = n
	notes.All.Add(n)
}

func (notes *NoteSystem) GetNote(scale int) *Note {
	n := notes.ByScale[scale]
	if n == nil && scale < 0 {
		n = notes.GetNote(scale + 12)
	} else if n == nil && scale > 0 {
		n = notes.GetNote(scale - 12)
	}
	return n
}

func (notes *NoteSystem) FindNote(name string) *Note {
	return notes.ByName[name]
}

func (notes *NoteSystem) FindNotes(notenames []string) NoteList {
	notelist := make(NoteList, len(notenames))
	for i, s := range notenames {
		notelist[i] = notes.FindNote(s)
	}
	return notelist
}
