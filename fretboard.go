package main

type Fretboard struct {
	FretCount int
	Strings   NoteList
}

func (fb *Fretboard) StringCount() int {
	return len(fb.Strings)
}

func NewFretboard(frets int, strings ...string) *Fretboard {
	return &Fretboard{frets, Notes.FindNotes(strings)}
}

var Ukulele = NewFretboard(12, "G", "C", "E", "A")
