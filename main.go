package main

import (
	"flag"
	"fmt"
	"os"
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage:\n%s [options] [key-chord]\nOptions:\n", os.Args[0])
	flag.PrintDefaults()
}

func contains(list []string, s string) bool {
	for _, i := range list {
		if i == s {
			return true
		}
	}
	return false
}

func main() {
	solver := NewSolver(Ukulele)
	flag.IntVar(&solver.MinFret, "min", 0, "Minimum fret to search chords")
	flag.IntVar(&solver.MaxFret, "max", 9, "Maximum fret to search chords")
	flag.IntVar(&solver.MaxDist, "width", 4, "Maximum 'width' of a chord in fret spaces")

	var kname = flag.String("key", "", "Key to generate chords for")
	var chname = flag.String("chord", "", "Chord to generate")

	flag.Usage = usage
	flag.Parse()

	kchnames := flag.Args()

	for _, chord := range AllChords {

		if *chname != "" && *chname != chord.Name {
			continue
		}

		for _, note := range Notes.All {

			if *kname != "" && *kname != note.Name {
				continue
			}

			if note.Scale < -5 || note.Scale > 11 {
				continue
			}

			var nch = note.Name + chord.Name

			if len(kchnames) > 0 && !contains(kchnames, nch) {
				continue
			}

			fmt.Printf("%s: \n", nch)

			scale := chord.GetNotes(note)

			for i, n := range scale {
				if i == 0 && len(solver.Frets.Strings) < len(chord.Intervals) {
					fmt.Printf("(%s) ", n.Name)
				} else {
					fmt.Printf("%s ", n.Name)

				}

			}

			fmt.Println()

			fingerings := solver.GetFingeringsByNote(note, chord)

			for i := 0; i < len(fingerings); i++ {
				for j := 0; j < len(fingerings[i]); j++ {
					fmt.Printf("%d ", fingerings[i][j])
				}
				fmt.Println()
			}

			fmt.Println()
		}
	}
}
